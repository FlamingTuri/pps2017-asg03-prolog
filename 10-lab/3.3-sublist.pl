% sublist(List1,List2)
% List1 should be a subset of List2
% example: sublist([1,2],[5,3,2,1]).

:- consult('1.1-search.pl').

sublist([],_).
sublist([X|Xs],[Y|Ys]) :-
   search(X,[Y|YS]),
   sublist(Xs,[Y|YS]),!.
   