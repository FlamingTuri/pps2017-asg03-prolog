% Test next

% [x,o,n]
% [x,x,n]
% [o,x,o]
% next([[x,o,n],[x,x,n],[o,x,o]],x,Result,NewTable).
%     -> Result / nothing  NewTable / [[x,o,x],[x,x,n],[o,x,o]]
%     -> Result / win(x)  NewTable / [[x,o,n],[x,x,x],[o,x,o]]

% [x,x,n]
% [x,o,n]
% [o,x,o]
% next([[x,x,n],[x,o,n],[o,x,o]],o,Result,NewTable).
%     -> Result / win(o)  NewTable / [[x,x,o],[x,o,n],[o,x,o]]
%     -> Result / nothing  NewTable / [[x,x,n],[x,o,o],[o,x,o]]

% [n,o,n]
% [x,o,x]
% [o,x,n]
% next([[n,o,n],[x,o,x],[o,x,n]],x,Result,NewTable).
%     -> Result / nothing  NewTable / [[x,o,n],[x,o,x],[o,x,n]]
%     -> Result / nothing  NewTable / [[n,o,x],[x,o,x],[o,x,n]]
%     -> Result / nothing  NewTable / [[n,o,n],[x,o,x],[o,x,x]]

% [o,n,n]
% [x,x,n]
% [o,x,o]
% next([[o,n,n],[x,x,n],[o,x,o]],x,Result,TableList).
%     -> Result / win(x)  TableList / [[o,x,n],[x,x,n],[o,x,o]]
%     -> Result / nothing  TableList / [[o,n,x],[x,x,n],[o,x,o]]
%     -> Result / win(x)  TableList / [[o,n,n],[x,x,x],[o,x,o]]

% test every possible move
% next([[n,n,n],[n,n,n],[n,n,n]],x,Result,NewTable). -> Result / nothing

:- consult('next.pl').
