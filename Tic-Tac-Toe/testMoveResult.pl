% Test moveResult

% [x,x,x]
% [x,o,n]
% [o,x,o]
% moveResult([[x,x,x],[x,o,n],[o,x,o]],Result). -> Result / win(x)

% [o,n,n]
% [x,x,x]
% [o,x,o]
% moveResult([[o,n,n],[x,x,x],[o,x,o]],Result). -> Result / win(x)

% [n,n,n]
% [o,o,n]
% [x,x,x]
% moveResult([[n,n,n],[o,o,n],[x,x,x]],Result). -> Result / win(x)

% [x,n,x]
% [x,o,n]
% [x,o,o]
% moveResult([[x,n,x],[x,o,n],[x,o,o]],Result). -> Result / win(x)

% [o,x,x]
% [n,x,n]
% [o,x,o]
% moveResult([[o,x,x],[n,x,n],[o,x,o]],Result). -> Result / win(x)

% [x,x,o]
% [x,o,n]
% [o,x,o]
% moveResult([[x,x,o],[x,o,n],[o,x,o]],Result). -> Result / win(o)

% [x,x,o]
% [o,x,x]
% [x,o,o]
% moveResult([[x,x,o],[o,x,x],[x,o,o]],Result). -> Result / even

% [o,n,x]
% [x,x,n]
% [o,x,o]
% moveResult([[o,n,x],[x,x,n],[o,x,o]],Result). -> Result / nothing

% [x,n,o]
% [n,x,n]
% [n,n,n]
% moveResult([[x,n,o],[n,x,n],[n,n,n]],Result). -> Result / nothing

:- consult('moveResult.pl').
