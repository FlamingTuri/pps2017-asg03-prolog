% Test game


% [x,o,x]
% [x,o,n]
% [o,x,o]
% game([[x,o,x],[x,o,n],[o,x,o]],x,Result,TableList).
%     -> Result / even  TableList / [[x,x,o],[x,o,x],[o,x,o]]

% [x,o,n]
% [x,x,n]
% [o,x,o]
% game([[x,o,n],[x,x,n],[o,x,o]],x,Result,TableList).
%     -> Result / win(x)  TableList / [[[x,o,n],[x,x,x],[o,x,o]]]
%     -> Result / even  TableList / [[[x,o,x],[x,x,n],[o,x,o]],[[x,o,x],[x,x,o],[o,x,o]]]

% [x,o,n]
% [x,n,o]
% [o,x,o] 
% game([[x,o,n],[x,n,o],[o,x,o]],x,Result,TableList).
%     -> Result / even  TableList / [[[x,o,x],[x,n,o],[o,x,o]],[[x,o,x],[x,o,o],[o,x,o]]]
%     -> Result / win(o)  TableList / [[[x,o,n],[x,x,o],[o,x,o]],[[x,o,o],[x,x,o],[o,x,o]]]

% [x,x,n]
% [x,o,n]
% [o,x,o] 
% game([[x,x,n],[x,o,n],[o,x,o]],x,Result,TableList).
%     -> Result/win(x)  TableList / [[[x,x,x],[x,o,n],[o,x,o]]]
%     -> Result/win(o)  TableList / [[[x,x,n],[x,o,x],[o,x,o]],[[x,x,o],[x,o,x],[o,x,o]]]

% [o,n,n]
% [x,x,n]
% [o,x,o]
% game([[o,n,n],[x,x,n],[o,x,o]],x,Result,TableList).
%     -> Result / win(x)  TableList / [[[o,x,n],[x,x,n],[o,x,o]]]
%     -> Result / win(x)  TableList / [[[o,n,n],[x,x,x],[o,x,o]]]
%     -> Result / win(x)  TableList / [[[o,n,x],[x,x,n],[o,x,o]],[[o,o,x],[x,x,n],[o,x,o]],[[o,o,x],[x,x,x],[o,x,o]]]
%     -> Result / win(x)  TableList / [[[o,n,x],[x,x,n],[o,x,o]],[[o,n,x],[x,x,o],[o,x,o]],[[o,x,x],[x,x,o],[o,x,o]]]

% test every possible game
% game([[n,n,n],[n,n,n],[n,n,n]],x,Result,TableList).

:- consult('game.pl').
