% insert(+Row,+Player,-NewRow)

insert([n|T],P,[P|T]) :- member(P,[x,o]).
insert([H|T],P,[H|R]) :- insert(T,P,R).