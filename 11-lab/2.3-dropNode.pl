% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node

% dropNode([e(1,2),e(3,3),e(1,3),e(2,1),e(13,1),e(2,3)],1,[e(3,3),e(2,3)]).

dropAll([],X,[]) :- !.
dropAll([XC|T],X,L) :- 
    copy_term(X,XC),
    dropAll(T,X,L),!.
dropAll([H|T],X,[H|L]) :- dropAll(T,X,L).

dropNode(G,N,O):-
    dropAll(G,e(N,_),G2),
    dropAll(G2,e(_,N),O).


% dropNodeWithCodeSmell([e(1,2),e(3,3),e(1,3),e(2,1),e(13,1),e(2,3)],1,[e(3,3),e(2,3)]).

dropAllStarting([],X,[]) :- !.
dropAllStarting([e(X,K)|T],X,L) :- 
    dropAllStarting(T,X,L),!.
dropAllStarting([e(H,K)|T],X,[e(H,K)|L]) :- 
    dropAllStarting(T,X,L).

dropAllLeaving([],X,[]) :- !.
dropAllLeaving([e(H,X)|T],X,L) :- 
    dropAllLeaving(T,X,L),!.
dropAllLeaving([e(H,K)|T],X,[e(H,K)|L]) :- 
    dropAllLeaving(T,X,L).

dropNodeWithCodeSmell(G,N,O):-
    dropAllStarting(G,N,G2),
    dropAllLeaving(G2,N,O).
