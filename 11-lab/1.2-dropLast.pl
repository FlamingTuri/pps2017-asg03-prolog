% dropLast(?Elem,?List,?OutList)
% drops last occurrence of Elem

% test
% dropLast(10,[10,20,10,30,10],L). -> L / [10,20,10,30]

dropLast(X,[H|Xs],[H|L]) :- dropLast(X,Xs,L),!.
dropLast(X,[X|T],T).
