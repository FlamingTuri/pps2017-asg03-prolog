% search_anytwo(Elem,List)
% looks for any Elem that occurs two times

% example
% search_anytwo(a,[b,c,a,a,d,e]).
% search_anytwo(a,[b,c,a,d,e,a,d,e]).

:- consult('1.1-search.pl').

search_anytwo(X,[X|Xs]):-search(X,Xs).
search_anytwo(X,[_|Xs]):-search_anytwo(X,Xs).
