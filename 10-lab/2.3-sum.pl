% sum(List,Sum)

% test
% sum([1,2,3],X). -> X / 6

sum([X],X).
sum([H|T],X):-sum(T,K), X is K+H.
