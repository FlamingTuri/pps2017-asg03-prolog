% dropFirst(?Elem,?List,?OutList)
% drops first occurrence of Elem

% test
% dropFirst(10,[10,20,10,30,10],L). -> L / [20,10,30,10]

dropFirst(X,[X|T],T) :- !.
dropFirst(X,[H|Xs],[H|L]):-dropFirst(X,Xs,L).
