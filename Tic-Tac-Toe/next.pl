:- consult('checkInput.pl').
:- consult('insert.pl').
:- consult('moveResult.pl').

% Player must be x or o
% next(@Table,@Player,-Result,-NewTable)

next([X,Y,Z],P,Result,[OX,Y,Z]) :- checkInput([X,Y,Z],P),insert(X,P,OX),moveResult([OX,Y,Z],Result).
next([X,Y,Z],P,Result,[X,OY,Z]) :- checkInput([X,Y,Z],P),insert(Y,P,OY),moveResult([X,OY,Z],Result).
next([X,Y,Z],P,Result,[X,Y,OZ]) :- checkInput([X,Y,Z],P),insert(Z,P,OZ),moveResult([X,Y,OZ],Result).
