% allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!

% allreaching([e(1,2),e(1,3),e(2,4)],1,L). -> L/[2,4,3])
% allreaching([e(1,2),e(1,3),e(1,3),e(2,4)],1,L). -> L/[2,4,3,3]
% allreaching([e(1,2),e(2,3),e(2,5),e(3,4),e(3,5)],1,L). -> L/[2,3,4,5,5]

allreaching([], N, []).
allreaching([e(N,H2)|T], N, [H2|L]) :- 
    allreaching(T, H2, L1),
    allreaching(T, N, L2),
    append(L1,L2,L),!.
allreaching([e(H1,H2)|T], N, L) :- allreaching(T, N, L).


% Solution with findall and anyPath
% findallreaching([e(1,2),e(1,3),e(2,4)],1,L). -> L/[2,3,4])
% findallreaching([e(1,2),e(1,3),e(1,3),e(2,4)],1,L). -> L/[2,3,3,4]
% findallreaching([e(1,2),e(2,3),e(2,5),e(3,4),e(3,5)],1,L). -> [2,3,5,4,5].

anypath(G, N1, N2, [e(N1,N2)]) :- member(e(N1, N2),G).
anypath(G, N1, N2, [e(N1,N3)|L]) :- member(e(N1,N3),G),anypath(G, N3, N2, L).

findallreaching(G,N,L) :- findall(X,anypath(G,N,X,_),L),!.
