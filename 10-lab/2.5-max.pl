% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element

max(List,Max) :- 
		List = [X|Xs],
		max([X|Xs],Max,X).
max([],Max,TempMax) :- Max is TempMax.

% version 1
max([X|Xs],Max,TempMax) :-
		write('X: '),write(X),nl,
		write('TempMax: '),write(TempMax),nl,
		(X > TempMax -> max(Xs,Max,X) ; max(Xs,Max,TempMax)).

% version 2
/*
max([X|Xs],Max,TempMax) :-
    (X > TempMax -> TempMax2 is X ; TempMax2 is TempMax),
		write('X: '),write(X),nl,
		write('TempMax: '),write(TempMax),nl,
		max(Xs,Max,TempMax2).
*/

% version 3
/*
max([Max],Max):-!.
max([X|Xs],Max) :-
		max(Xs,TailMax),
    (X > TailMax -> Max is X; Max is TailMax).
*/