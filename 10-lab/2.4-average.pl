% average(List,Average)
% it uses average(List,Count,Sum,Average)
% this is a tail recursion

% average([3,4,3],A). -> A / 3
% average([3,4,3],0,0,A). -> A / 3
% average([4,3],1,3,A). -> A / 3
% average([3],2,7,A). -> A / 3
% average([],3,10,A). -> A=3.3333

average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-
    C2 is C+1,
    S2 is S+X,
    average(Xs,C2,S2,A).
