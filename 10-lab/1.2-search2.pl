% search2(Elem,List)
% looks for two consecutive occurrences of Elem

% test
% search2(a,[b,c,a,a,d,e,a,a,g,h]).
% search2(a,[b,c,a,a,a,d,e]).
% search2(X,[b,c,a,a,d,d,e]).
% search2(a,L).
% search2(a,[_,_,a,_,a,_]).

search2(X,[X,X|_]).
search2(X,[_|Xs]):-search2(X,Xs).
