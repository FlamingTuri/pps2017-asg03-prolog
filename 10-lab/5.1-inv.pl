% inv(List,RList)
% example: inv([1,2,3],[3,2,1]).

cutlast([Z],Z,[]) :- !.
cutlast([H|T],Z,[H|CL]) :- cutlast(T,Z,CL).

inv([],[]) :- !.
inv([X|Xs],Rlist) :-
		cutlast(Rlist,X,CL),
		inv(Xs,CL).


% using built-in reverse function
% inv(List,Rlist) :- reverse(Rlist, List),!.
