% search(Elem,List)

% test
% search(a,[a,b,c]). 
%   -> yes.
%   -> no.
% search(a,[c,d,e]). -> no.
% search(X,[a,b,c]).
%   -> X / a
%   -> X / b
%   -> X / c
% search(a,X).
% search(a,[X,b,Y,Z]).
%   -> X / a
%   -> Y / a
%   -> Z / a
% search(X,Y).

search(X,[X|_]).
search(X,[_|Xs]):-search(X,Xs).
