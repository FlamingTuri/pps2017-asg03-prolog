:- consult('next.pl').
:- consult('nextTurn.pl').

% game(@Table,@Player,-Result,-TableList)

game(Table,P,win(P),[NewTable]) :- checkInput(Table,P),next(Table,P,Result,NewTable),Result == win(P).
game(Table,P,even,[NewTable]) :- checkInput(Table,P),next(Table,P,Result,NewTable),Result == even.
game(Table,P,Result,[NewTable|TableList]) :-
    checkInput(Table,P), % probably unnecessary
    next(Table,P,TempResult,NewTable),
    TempResult == nothing,
    nextTurn(P,N),
    game(NewTable,N,Result,TableList).
