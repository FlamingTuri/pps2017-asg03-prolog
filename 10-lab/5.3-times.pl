% times(?List,+N,?NList)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).

% test
% times([1,2,3],3,X). -> X / [1,2,3,1,2,3,1,2,3]
% times(X,3,[1,2,3,1,2,3,1,2,3]). -> X / [1,2,3]


times(List,1,List). 
times(List,N,NList) :-
    (N > 1 -> 
        N2 is N-1,
        append(List,R,NList),
        times(List,N2,R),!).
