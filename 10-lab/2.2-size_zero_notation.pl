% size(List,Size)
% Size will contain the number of elements in List,
% written using notation zero, s(zero), s(s(zero))..

% test
% size([a,b,c],X). -> X/s(s(s(zero)))
% size(L,s(s(s(zero)))).

size([],zero).
size([_|T],s(M)):-size(T,M).