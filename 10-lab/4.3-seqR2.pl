% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).

%append([],L,L).
%append([H|T],L,[H|M]):- append(T,L,M).

cut_last(L1, L2) :- append(L2, [_], L1).

last([H],L) :- L is H.
last([H|T],L) :- last(T,L).

seqR2(0,[0]).
seqR2(N,[X|T]) :- 
    last([H|T],N),
    cut_last([X|T],NoLast),
    N2 is N-1, seqR2(N2,NoLast),!.
