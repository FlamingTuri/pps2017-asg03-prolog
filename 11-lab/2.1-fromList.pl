% fromList(+List,-Graph)

% examples
% fromList([10,20,30],[e(10,20),e(20,30)]).
% fromList([10,20],[e(10,20)]).
% fromList([10],[]).

fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):-fromList([H2|T],L).
