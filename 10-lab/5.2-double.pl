% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).

double(List,DList) :- append(List,List,DList),!.
