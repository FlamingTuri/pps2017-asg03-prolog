% moveResult(+Table,Result)

% horizontal 
moveResult([[P,P,P]|T], win(P)) :- member(P,[x,o]),!.
moveResult([[_,_,_]|T], MoveResult) :- moveResult(T, MoveResult),!.

% vertical
moveResult([[P|T1],[P|T2],[P|T3]], win(P)) :- member(P,[x,o]),!.
moveResult([[_|T1],[_|T2],[_|T3]], MoveResult) :- moveResult([T1,T2,T3], MoveResult),!.

% diagonal
moveResult([[P,_,_],
            [_,P,_],
            [_,_,P]], win(P)) :- member(P,[x,o]),!.
moveResult([[_,_,P],
            [_,P,_],
            [P,_,_]], win(P)) :- member(P,[x,o]),!.

% nothing/even
moveResult([[X1,X2,X3],[Y1,Y2,Y3],[Z1,Z2,Z3]], Result) :- 
    ((member(n,[X1,X2,X3]); member(n,[Y1,Y2,Y3]); member(n,[Z1,Z2,Z3])) 
        -> Result = nothing; Result = even).
