% fromCircList(+List,-Graph)

% examples
% fromCircList([10,20,30],[e(10,20),e(20,30),e(30,10)]).
% fromCircList([10,20],[e(10,20),e(20,10)]).
% fromCircList([10],[e(10,10)]).

fromCircList([L],F,[e(L,F)]).
fromCircList([H1,H2|T],F,[e(H1,H2)|L]) :- fromCircList([H2|T],F,L).
fromCircList([H1,H2|T],[e(H1,H2)|L]) :- fromCircList([H2|T],H1,L).

fromCircList([H1],[e(H1,H1)]).
