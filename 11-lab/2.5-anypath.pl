% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1

% test
% anypath([e(1,2),e(1,3),e(1,4),e(2,3)],1,3,L).
%   – L/[e(1,2),e(2,3)]
%   – L/[e(1,3)]
% anypath([e(1,2),e(2,3),e(2,5),e(3,4),e(3,5)],1,5,L).
%   – L/[e(1,2),e(2,3),e(3,5)]
%   – L/[e(1,2),e(2,5)]

anypath(G, N1, N2, [e(N1,N2)]) :- member(e(N1, N2),G).
anypath(G, N1, N2, [e(N1,N3)|L]) :- member(e(N1,N3),G),anypath(G, N3, N2, L).

/*
anypath(G, N1, N2, [e(N1,N3)|L]) :- member(e(N1,N3),G),anypath(G, N3, N2, L).
anypath(G, N1, N2, [e(N1,N2)]) :- member(e(N1, N2),G),!.
*/
