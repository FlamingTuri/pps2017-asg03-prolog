% dropAll(?Elem,?List,?OutList)
% drops all occurrences of Elem

% test
% dropAll(10,[10,20,10,30,10],L). -> L / [20,30]

dropAll(X,[],[]) :- !.
dropAll(X,[X|T],L) :- dropAll(X,T,L),!.
dropAll(X,[H|T],[H|L]) :- dropAll(X,T,L).

dropAllv2(X,[],[]) :- !.
dropAllv2(X,[X|T],Y) :- !, dropAllv2(X,T,Y).
dropAllv2(X,[H|T],Y) :- !, dropAllv2(X,T,Y2), append([H],Y2,Y).