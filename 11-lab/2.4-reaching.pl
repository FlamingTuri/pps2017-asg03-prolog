% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node

% test
% reaching([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3].
% reaching([e(1,2),e(1,2),e(2,3)],1,L). -> L/[2,2].
% reaching([e(1,2),e(2,3),e(1,4)],1,L). -> L/[2,4].

reaching([],N,[]) :- !.
reaching([e(N,H2)|T],N,[H2|L]) :- reaching(T,N,L),!.
reaching([e(H1,H2)|T],N,L) :- reaching(T,N,L).

% possibly use findall, looking for e(Node,_) combined
% with member(?Elem,?List)

% test
% reachingv2([e(1,2),e(1,3),e(2,3)],1,L). -> L/[2,3].
% reachingv2([e(1,2),e(1,2),e(2,3)],1,L). -> L/[2,2].
% reachingv2([e(1,2),e(2,3),e(1,4)],1,L). -> L/[2,4].

reachingv2(G,N,L) :- findall(K, member(e(N,K),G), L).
